package com.example.mapdemo;

import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        System.out.println(exchangeDistance(1000.5));
        System.out.println(exchangeDistance(101900.5));
    }

    public String exchangeTime(int time) {
        time = time/60;
        if(time < 60){
            return time + "分钟";
        } else if(time >= 60 && time < 24*60){
            if(time%60 == 0){
                return time/60+"小时";
            } else {
                return time/60+"小时"+time%60+"分钟";
            }
        } else {
            if((time%(24*60))/60 == 0){
                return time/(24*60)+"天";
            } else {
                return time/(24*60)+"天"+(time%(24*60))/60+"小时";
            }
        }
    }

    public String exchangeDistance(double distance) {
        if (distance >= 0 && distance < 1) {
            return "距离1米";
        } else if(distance >= 1 && distance < 1000) {
            return "距离"+Math.round(distance)+"米";
        } else if(distance >= 1000 && distance < 100000) {
            if(distance%1000 >= 100){
                return "距离"+String.format("%.1f", distance/1000)+"公里";
            } else {
                return "距离"+(int)distance/1000+"公里";
            }
        } else {
            return "距离"+Math.round(distance/1000)+"公里";
        }
    }
}