package com.example.mapdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.baidu.mapapi.search.core.PoiInfo;

import java.util.List;

public class PoiAdapter extends ArrayAdapter<PoiInfo> implements OnClickListener {

    private int resourceId;
    private Callback mCallback;
    private LayoutInflater mInflater;

    public interface Callback {
        public void click(View v);
    }

    public PoiAdapter(Context context, int textViewResourceId, List<PoiInfo> object, Callback callback) {
        super(context, textViewResourceId, object);
        resourceId = textViewResourceId;
        mCallback = callback;
    }

    public View getView(int posotion, View convertView, ViewGroup parent) {


        PoiInfo poiInfo = getItem(posotion);
        View view = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
        TextView name = view.findViewById(R.id.name);
        TextView address = view.findViewById(R.id.address);
        name.setText((posotion+1)+" "+poiInfo.name);
        address.setText(poiInfo.area+" "+poiInfo.address.replace(poiInfo.city+poiInfo.area, ""));
        Button button = view.findViewById(R.id.jump);
        if(button != null){
            button.setTag(posotion);
        }
        button.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View view) {
        mCallback.click(view);
    }
}
