package com.example.mapdemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchResult;
import com.baidu.mapapi.search.poi.PoiIndoorResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.IndoorRouteResult;
import com.baidu.mapapi.search.route.MassTransitRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.baidu.mapapi.utils.DistanceUtil;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BaiduNaviManagerFactory;
import com.baidu.navisdk.adapter.IBNRoutePlanManager;
import com.baidu.navisdk.adapter.IBaiduNaviManager;
import com.example.mapdemo.PoiAdapter.Callback;
import com.example.mapdemo.overlayutil.DrivingRouteOverlay;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements OnItemClickListener, Callback {

    private TextView progressText = null;
    private TextView searchZero = null;
    private ProgressBar progressBar = null;
    private ProgressDialog progressDialog = null;
    private boolean isFirstLocation = true;
    private MapView mMapView = null;
    private BaiduMap mBaiduMap = null;
    private LocationClient mLocationClient = null;
    private PoiSearch mPoiSearch = null;
    private BDLocation allLocation = null;
    private RoutePlanSearch mSearch = null;
    private MyLocationListener myLocationListener = new MyLocationListener();
    private AlertDialog driverDialog = null;
    private PoiResult allPoiResult = null;
    private List<PoiInfo> data = new ArrayList<>();
    private ListView listView = null;
    private View drive = null;
    private AlertDialog listDialog = null;
    private String authinfo = null;


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void click(View v) {
        List<PoiInfo> list = allPoiResult.getAllPoi();
        hideInput(listDialog);
        Integer tag = (Integer)v.getTag();
        PoiInfo poiInfo = list.get(tag);
        getDrivingRoute(poiInfo.city, poiInfo.address, poiInfo.location);
    }


    /**
     * 注册定位监听器
     */
    public class MyLocationListener extends BDAbstractLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            //mapView 销毁后不在处理新接收的位置
            if (location == null || mMapView == null){
                return;
            }
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(location.getDirection()).latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
            //保存当前位置信息
            allLocation = location;
            //初次进入程序跳转图层到当前位置
            if(isFirstLocation){
                jumpMyLocation();
                isFirstLocation = false;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button reset = findViewById(R.id.reset);
        Button search = findViewById(R.id.search);
        Button setting = findViewById(R.id.setting);

        reset.setOnClickListener(view -> {
            reset.setVisibility(View.INVISIBLE);
            if (mMapView.getMapLevel() >= 10000){
                mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(16));
            }
            jumpMyLocation();
            mBaiduMap.clear();
        });

        search.setOnClickListener(view -> {
            showListDialog();
        });

        setting.setOnClickListener(view -> {
            View settingDialogView = getLayoutInflater().inflate(R.layout.setting_dialog_layout, null);

            Button close = settingDialogView.findViewById(R.id.close);

            AlertDialog infoDialog = new AlertDialog.Builder(this)
                    .setView(settingDialogView)
                    .create();
            Window infoDialogWindow = infoDialog.getWindow();
            infoDialogWindow.getAttributes().gravity = Gravity.TOP | Gravity.LEFT;
            infoDialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            infoDialogWindow.setBackgroundDrawableResource(R.drawable.shape_bg_waring);

            close.setOnClickListener(v -> {
                infoDialog.dismiss();
            });

            infoDialog.show();
        });


        mMapView = findViewById(R.id.bmapView);
        mBaiduMap = mMapView.getMap();

        mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(17));
        mBaiduMap.setMyLocationEnabled(true);

        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {

            @Override
            public void onMapStatusChangeStart(MapStatus status) {

            }

            @Override
            public void onMapStatusChangeStart(MapStatus status, int reason) {
                if(reason == 1){
                    reset.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onMapStatusChange(MapStatus status) {

            }

            @Override
            public void onMapStatusChangeFinish(MapStatus status) {

            }
        });

        //定位初始化
        mLocationClient = new LocationClient(this);

        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true); // 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
        option.setScanSpan(1000);
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);

        mLocationClient.registerLocationListener(myLocationListener);
        //开启地图定位图层

        mLocationClient.start();

        mPoiSearch = PoiSearch.newInstance();

        mPoiSearch.setOnGetPoiSearchResultListener(new OnGetPoiSearchResultListener() {
            @Override
            public void onGetPoiResult(PoiResult poiResult) {
                allPoiResult = poiResult;
                if(poiResult.getAllPoi() == null || poiResult.getAllPoi().size() == 0){
                    listView.setAdapter(null);
                    searchZero.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    progressText.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                } else{
                    listView.setAdapter(new PoiAdapter(MainActivity.this,R.layout.list_item_layout, poiResult.getAllPoi(),MainActivity.this));
                    progressBar.setVisibility(View.GONE);
                    progressText.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onGetPoiDetailResult(PoiDetailSearchResult poiDetailSearchResult) {

            }
            @Override
            public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {

            }
            @Override
            public void onGetPoiDetailResult(PoiDetailResult poiDetailResult) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();

    }
    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();

    }
    @Override
    protected void onDestroy() {
        mLocationClient.stop();
        mBaiduMap.setMyLocationEnabled(false);
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
        mMapView = null;
        super.onDestroy();
        mPoiSearch.destroy();
    }

    private void showListDialog() {

        View listDialogView = getLayoutInflater().inflate(R.layout.list_dialog_layout, null);
        //初始化控件
        EditText editText = listDialogView.findViewById(R.id.input_message);
        Button close = listDialogView.findViewById(R.id.close);
        Button clearButton = listDialogView.findViewById(R.id.clear_text_button);
        Button search = listDialogView.findViewById(R.id.search);
        listView = listDialogView.findViewById(R.id.list_item);
        progressBar = listDialogView.findViewById(R.id.search_progress_bar);
        progressText = listDialogView.findViewById(R.id.search_progress_text);
        searchZero = listDialogView.findViewById(R.id.search_zero);

        //初始化提示框
        listDialog = new AlertDialog.Builder(this)
                .setView(listDialogView)
                .create();
        Window listDialogWindow = listDialog.getWindow();
        listDialogWindow.getAttributes().gravity = Gravity.TOP | Gravity.LEFT;
        listDialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        listDialogWindow.setBackgroundDrawableResource(R.drawable.shape_bg_waring);

        //关闭按钮监听器
        close.setOnClickListener(view -> {
            listDialog.dismiss();
        });

        //文本编辑框监听器
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // 输入之前的字符串，输入的位置，受影响的字符数，被影响后
                Log.i("before",",start="+start+",count="+count+",after="+after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 输入之中的字符串，输入的位置，被影响后，受影响的字符数
                Log.i("on",",start="+start+",count="+count+",before="+before);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0){
                    clearButton.setVisibility(View.VISIBLE);
                    searchZero.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    progressText.setVisibility(View.VISIBLE);
                    if(s != null){
                        Log.i("after","Editable="+s+allLocation.getCity()); //-------------------------------
                        mPoiSearch.searchInCity(new PoiCitySearchOption()
                                .city(allLocation.getCity()) //必填
                                .keyword(s.toString()) //必填
                                .pageNum(0));
                    }
                }else{
                    searchZero.setVisibility(View.GONE);
                    clearButton.setVisibility(View.GONE);
                    listView.setAdapter(null);
                }
                // 输入之后的字符串
            }
        });

        //文本编辑器清空按钮监听器
        clearButton.setOnClickListener(view1 -> {
            editText.setText("");
            editText.requestFocusFromTouch();
        });

        //初始化适配器
        PoiAdapter adapter = new PoiAdapter(this, R.layout.list_item_layout, data, this);
        //通过适配器将列表载入列表控件
        listView.setAdapter(adapter);
        //设置列表控件项监听器
        listView.setOnItemClickListener((AdapterView<?> var1, View var2, int index, long var4) -> {
            //关闭软键盘
            hideInput(listDialog);

            View infoDialogView = getLayoutInflater().inflate(R.layout.info_dialog_layout, null);

            TextView address = infoDialogView.findViewById(R.id.address);
            TextView title = infoDialogView.findViewById(R.id.title);
            TextView distance = infoDialogView.findViewById(R.id.distance);
            Button back = infoDialogView.findViewById(R.id.back_button);
            Button jump = infoDialogView.findViewById(R.id.jump);

            AlertDialog infoDialog = new AlertDialog.Builder(this)
                    .setView(infoDialogView)
                    .create();
            Window infoDialogWindow = infoDialog.getWindow();
            infoDialogWindow.getAttributes().gravity = Gravity.TOP | Gravity.LEFT;
            infoDialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            infoDialogWindow.setBackgroundDrawableResource(R.drawable.shape_bg_waring);

            //返回按钮监听器
            back.setOnClickListener(view -> {
                infoDialog.dismiss();
            });

            //设置标题
            title.setText(allPoiResult.getAllPoi().get(index).name);

            //设置地址
            address.setText(allPoiResult.getAllPoi().get(index).address);

            //设置距离
            LatLng startLocation = new LatLng(allLocation.getLatitude(),allLocation.getLongitude());
            LatLng endLocation = allPoiResult.getAllPoi().get(index).getLocation();
            double dis = DistanceUtil. getDistance(startLocation, endLocation);
            distance.setText(exchangeDistance(dis));

            //设置寻路按钮监听器
            jump.setOnClickListener(vb -> {
                infoDialog.dismiss();
                List<PoiInfo> list = allPoiResult.getAllPoi();
                getDrivingRoute(list.get(index).city, list.get(index).address, list.get(index).getLocation());
            });

            infoDialog.show();
        });

        listDialog.show();

    }


    private void getDrivingRoute(String city, String address, LatLng location){

        mSearch = RoutePlanSearch.newInstance();

        OnGetRoutePlanResultListener listener = new OnGetRoutePlanResultListener() {

            @Override
            public void onGetWalkingRouteResult(WalkingRouteResult walkingRouteResult) {

            }

            @Override
            public void onGetTransitRouteResult(TransitRouteResult transitRouteResult) {

            }

            @Override
            public void onGetMassTransitRouteResult(MassTransitRouteResult massTransitRouteResult) {

            }

            @Override
            public void onGetIndoorRouteResult(IndoorRouteResult indoorRouteResult) {

            }

            @Override
            public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {

            }

            @Override
            public void onGetDrivingRouteResult(DrivingRouteResult drivingRouteResult) {

                //创建DrivingRouteOverlay实例
                DrivingRouteOverlay overlay = new DrivingRouteOverlay(mBaiduMap);

                if (drivingRouteResult.getRouteLines() != null && drivingRouteResult.getRouteLines().size() > 0) {
                    jumpMyLocation();
                    mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(17));
                    //获取路径规划数据,(以返回的第一条路线为例）
                    //为DrivingRouteOverlay实例设置数据
                    overlay.setData(drivingRouteResult.getRouteLines().get(0));
                    //在地图上绘制DrivingRouteOverlay

                    mBaiduMap.clear();
                    overlay.addToMap();

                    LatLng startLocation = new LatLng(allLocation.getLatitude(),allLocation.getLongitude());
                    double distance = DistanceUtil. getDistance(startLocation, location);
                    int time = 0;

                    time = drivingRouteResult.getRouteLines().get(0).getDuration();

                    showDriverDialog(address, distance, location, time);

                } else {
                    driverDialog.dismiss();
                    if(isNetworkConnected(MainActivity.this)){
                        Toast.makeText(MainActivity.this, "路线请求失败，请重试", Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(MainActivity.this, "网络连接出错，请重试", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };


        mSearch.setOnGetRoutePlanResultListener(listener);

        Log.d("寻路",allLocation.getCity()+" "+allLocation.getAddress().address.replace(allLocation.getCountry(),""));
        Log.d("寻路",city+" "+address);

        PlanNode stNode = PlanNode.withLocation(new LatLng(allLocation.getLatitude(),allLocation.getLongitude()));
        PlanNode enNode = PlanNode.withCityNameAndPlaceName(city, address);

        mSearch.drivingSearch((new DrivingRoutePlanOption())
                .from(stNode)
                .to(enNode));
        mSearch.destroy();


        drive = getLayoutInflater().inflate(R.layout.driving_dialog_layout, null);
        driverDialog = new AlertDialog.Builder(MainActivity.this)
                .setView(drive)
                .create();
        Window driverDialogWindow = driverDialog.getWindow();
        driverDialogWindow.getAttributes().gravity = Gravity.TOP | Gravity.LEFT;
        driverDialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        driverDialogWindow.setBackgroundDrawableResource(R.drawable.shape_bg_waring);
        driverDialog.show();

    }


    private void showDriverDialog(String address, double distance, LatLng location, int time) {
        /*@setView 装入一个EditView
         */

        Button back_button = drive.findViewById(R.id.back_button);
        TextView distance_view = drive.findViewById(R.id.distance);
        TextView minute = drive.findViewById(R.id.minute);
        View progressBar = drive.findViewById(R.id.progress_view);
        View info_view = drive.findViewById(R.id.info_view);
        Button drive_button = drive.findViewById(R.id.drive_button);

        back_button.setOnClickListener(driver -> {
            driverDialog.dismiss();
        });

        minute.setText(exchangeTime(time));


        distance_view.setText(exchangeDistance(distance));

        drive_button.setOnClickListener(driver -> {
            start_driver(address, location);
        });

        info_view.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        driverDialog.show();

    }

    public void start_driver(String address, LatLng location){

        BaiduNaviManagerFactory.getBaiduNaviManager().init(this,
                "mSDCardPath", "APP_FOLDER_NAME", new IBaiduNaviManager.INaviInitListener() {

                    @Override
                    public void onAuthResult(int status, String msg) {
                        if (0 == status) {
                            authinfo = "key校验成功!";
                        } else {
                            authinfo = "key校验失败, " + msg;
                        }
                        MainActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, authinfo, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void initStart() {
                        Toast.makeText(MainActivity.this, "百度导航引擎初始化开始", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void initSuccess() {
                        Toast.makeText(MainActivity.this, "百度导航引擎初始化成功", Toast.LENGTH_SHORT).show();
                        start_run(address, location);
                    }

                    @Override
                    public void initFailed(int errCode) {
                        Toast.makeText(MainActivity.this, "百度导航引擎初始化失败", Toast.LENGTH_SHORT).show();
                    }

                });

    }

    public void start_run(String address, LatLng location){


        BNRoutePlanNode sNode = new BNRoutePlanNode.Builder()
                .latitude(allLocation.getLatitude())
                .longitude(allLocation.getLongitude())
                .name(allLocation.getAddrStr())
                .description(allLocation.getAddrStr())
//                .coordinateType(CoordinateType.GCJ02)
                .build();
        BNRoutePlanNode eNode = new BNRoutePlanNode.Builder()
                .latitude(location.latitude)
                .longitude(location.longitude)
                .name(address)
                .description(address)
//                .coordinateType(CoordinateType.GCJ02)
                .build();
        List<BNRoutePlanNode> list = new ArrayList<>();
        list.add(sNode);
        list.add(eNode);

        BaiduNaviManagerFactory.getRoutePlanManager().routePlanToNavi(
                list,
                IBNRoutePlanManager.RoutePlanPreference.ROUTE_PLAN_PREFERENCE_DEFAULT,
                null,
                new Handler(Looper.getMainLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_START:
                                Toast.makeText(MainActivity.this.getApplicationContext(),
                                        "算路开始", Toast.LENGTH_SHORT).show();
                                break;
                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_SUCCESS:
                                Toast.makeText(MainActivity.this.getApplicationContext(),
                                        "算路成功", Toast.LENGTH_SHORT).show();
                                break;
                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_FAILED:
                                Toast.makeText(MainActivity.this.getApplicationContext(),
                                        "算路失败", Toast.LENGTH_SHORT).show();
                                break;
                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_TO_NAVI:
                                Toast.makeText(MainActivity.this.getApplicationContext(),
                                        "算路成功准备进入导航", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(MainActivity.this, DemoGuideActivity.class);

                                startActivity(intent);

                                break;
                            default:
                                // nothing
                                break;
                        }
                    }
                });
    }


    private void jumpMyLocation() {
        LatLng ll = new LatLng(allLocation.getLatitude(), allLocation.getLongitude());
        MapStatusUpdate status = MapStatusUpdateFactory.newLatLng(ll);
        mBaiduMap.animateMapStatus(status);
    }

    private void hideInput(AlertDialog alertDialog) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View view = alertDialog.getWindow().peekDecorView();
        if (null != view) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    public String exchangeTime(int time) {
        time = time/60;
        if(time < 60){
            return time + "分钟";
        } else if(time >= 60 && time < 24*60){
            if(time%60 == 0){
                return time/60+"小时";
            } else {
                return time/60+"小时"+time%60+"分钟";
            }
        } else {
            if((time%(24*60))/60 == 0){
                return time/(24*60)+"天";
            } else {
                return time/(24*60)+"天"+(time%(24*60))/60+"小时";
            }
        }
    }

    public String exchangeDistance(double distance) {
        if (distance >= 0 && distance < 1) {
            return "距离1米";
        } else if(distance >= 1 && distance < 1000) {
            return "距离"+Math.round(distance)+"米";
        } else if(distance >= 1000 && distance < 100000) {
            if(distance%1000 >= 100){
                return "距离"+String.format("%.1f", distance/1000)+"公里";
            } else {
                return "距离"+(int)distance/1000+"公里";
            }
        } else {
            return "距离"+Math.round(distance/1000)+"公里";
        }
    }
}
