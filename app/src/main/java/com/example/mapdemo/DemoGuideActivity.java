/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.example.mapdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import com.baidu.navisdk.adapter.BNaviCommonParams;
import com.baidu.navisdk.adapter.BaiduNaviManagerFactory;
import com.baidu.navisdk.adapter.IBNRouteGuideManager;
import com.baidu.navisdk.adapter.IBNaviListener;
import com.baidu.navisdk.adapter.IBNaviViewListener;
import com.baidu.navisdk.adapter.struct.BNGuideConfig;
import com.baidu.navisdk.adapter.struct.BNHighwayInfo;
import com.baidu.navisdk.adapter.struct.BNRoadCondition;
import com.baidu.navisdk.adapter.struct.BNaviInfo;
import com.baidu.navisdk.adapter.struct.BNaviLocation;
import com.baidu.navisdk.adapter.struct.BNaviResultInfo;
import com.baidu.navisdk.ui.routeguide.model.RGLineItem;
import java.util.List;


/**
 * 诱导界面
 */
public class DemoGuideActivity extends FragmentActivity {

    private static final String TAG = DemoGuideActivity.class.getName();

    private IBNRouteGuideManager mRouteGuideManager;
    private IBNaviListener.DayNightMode mMode = IBNaviListener.DayNightMode.DAY;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = new Bundle();
        // IS_REALNAVI代表导航类型，true表示真实导航，false表示模拟导航，默认是true
        bundle.putBoolean(BNaviCommonParams.ProGuideKey.IS_REALNAVI, true);
        // IS_SUPPORT_FULL_SCREEN代表是否沉浸式，默认是true
        bundle.putBoolean(BNaviCommonParams.ProGuideKey.IS_SUPPORT_FULL_SCREEN, true);

        mRouteGuideManager = BaiduNaviManagerFactory.getRouteGuideManager();
        BNGuideConfig config = new BNGuideConfig.Builder()
                .params(bundle)
                .build();
        view = mRouteGuideManager.onCreate(this, config);

        if (view != null) {
            setContentView(view);
        }
        routeGuideEvent();
    }

    // 导航过程事件监听
    private void routeGuideEvent() {
        BaiduNaviManagerFactory.getRouteGuideManager().setNaviListener(new IBNaviListener() {

            @Override
            public void onRoadNameUpdate(String name) { // 弹窗展示
            }

            @Override
            public void onRemainInfoUpdate(int remainDistance, int remainTime) { // 弹窗展示
            }

            @Override
            public void onViaListRemainInfoUpdate(Message msg) {

            }

            @Override
            public void onGuideInfoUpdate(BNaviInfo naviInfo) { // 弹窗展示
            }

            @Override
            public void onHighWayInfoUpdate(Action action, BNHighwayInfo info) {
            }

            @Override
            public void onFastExitWayInfoUpdate(Action action, String name, int dist, String id) {
            }

            @Override
            public void onEnlargeMapUpdate(Action action, View enlargeMap, String remainDistance,
                                           int progress, String roadName, Bitmap turnIcon) {
            }

            @Override
            public void onDayNightChanged(DayNightMode style) {
            }

            @Override
            public void onRoadConditionInfoUpdate(double progress, List<BNRoadCondition> items) {
            }

            @Override
            public void onMainSideBridgeUpdate(int type) {
            }

            @Override
            public void onLaneInfoUpdate(Action action, List<RGLineItem> laneItems) {
            }

            @Override
            public void onSpeedUpdate(int i, int i1) {

            }


            @Override
            public void onArriveDestination() {
                BNaviResultInfo info =
                        BaiduNaviManagerFactory.getRouteGuideManager().getNaviResultInfo();
                Toast.makeText(DemoGuideActivity.this, "导航结算数据: " + info.toString(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onArrivedWayPoint(int index) {
            }

            @Override
            public void onLocationChange(BNaviLocation naviLocation) {
            }

            @Override
            public void onMapStateChange(MapStateMode mapStateMode) {
            }

            @Override
            public void onStartYawing(String s) {

            }

            @Override
            public void onYawingSuccess() {
            }

            @Override
            public void onYawingArriveViaPoint(int i) {

            }

            @Override
            public void onNotificationShow(String msg) {
                Log.e(TAG, msg);
            }

            @Override
            public void onHeavyTraffic() {
                Log.e(TAG, "onHeavyTraffic");
            }

            @Override
            public void onNaviGuideEnd() {
                DemoGuideActivity.this.finish();
            }
        });

        BaiduNaviManagerFactory.getRouteGuideManager().setNaviViewListener(
                new IBNaviViewListener() {
                    @Override
                    public void onMainInfoPanCLick() {
                    }

                    @Override
                    public void onNaviTurnClick() {
                    }

                    @Override
                    public void onFullViewButtonClick(boolean show) {
                    }

                    @Override
                    public void onFullViewWindowClick(boolean show) {
                    }

                    @Override
                    public void onNaviBackClick() {
                        Log.e(TAG, "onNaviBackClick");
                    }

                    @Override
                    public void onBottomBarClick(Action action) {
                    }

                    @Override
                    public void onNaviSettingClick() {
                        Log.e(TAG, "onNaviSettingClick");
                    }

                    @Override
                    public void onRefreshBtnClick() {
                    }

                    @Override
                    public void onZoomLevelChange(int level) {
                    }

                    @Override
                    public void onMapClicked(double x, double y) {
                    }

                    @Override
                    public void onMapMoved() {
                        Log.e(TAG, "onMapMoved");
                    }

                    @Override
                    public void onFloatViewClicked() {
                        try {
                            Intent intent = new Intent();
                            intent.setPackage(getPackageName());
                            intent.setClass(DemoGuideActivity.this,
                                    Class.forName(DemoGuideActivity.class.getName()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                            startActivity(intent);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void unInitTTSListener() {
        BaiduNaviManagerFactory.getTTSManager().setOnTTSStateChangedListener(null);
        BaiduNaviManagerFactory.getTTSManager().setOnTTSStateChangedHandler(null);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRouteGuideManager.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mRouteGuideManager.onResume();
    }

    protected void onPause() {
        super.onPause();
        mRouteGuideManager.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRouteGuideManager.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRouteGuideManager.onDestroy(false);
        unInitTTSListener();
        mRouteGuideManager = null;
    }

    @Override
    public void onBackPressed() {
        mRouteGuideManager.onBackPressed(false, true);
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mRouteGuideManager.onConfigurationChanged(newConfig);
    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {

    }

    @Override
    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (!mRouteGuideManager.onKeyDown(keyCode, event)) {
            return super.onKeyDown(keyCode, event);
        }
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        mRouteGuideManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRouteGuideManager.onActivityResult(requestCode, resultCode, data);
    }
}
